<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Change log for expect-exit

## 0.5.3 (2024-10-11)

- Mark this library as DEPRECATED.
  Its development is now in maintenance mode only!
- Declare MSRV 1.58, inline some format string arguments.
- Rearrange the way Clippy warnings are enabled and disabled;
  the recommended way is to run the `run-clippy.sh` tool.
- Do not use `impl Error` in a function argument, make the functions
  generic instead.
- Refactor both the library and the test suite code a bit for readability.
- Use the `anyhow`, `camino`, and `rstest` libraries in the test suite.
- Use crate versions in the Debian stable distribution for the dependencies.

## 0.5.2 (2022-10-10)

- Add an EditorConfig definition for POSIX shell script programs.
- Let the `run-clippy` tool pass additional arguments to Clippy.
- Explicitly allow the "unnecessary lazy evaluations" Clippy lint for
  the use of `.then()` instead of `.then_some()` - no need for MRV 1.62.
- Explicitly allow the "use of std instead of core" Clippy lint - there is
  not much chance of `expect-exit` gaining a `no-std` flavor anytime soon.
- Switch from `lazy_static` to `once_cell` in the test suite.

## 0.5.1 (2022-08-21)

- Keep the `Cargo.lock` file under version control.

## 0.5.0 (2022-08-15)

- Possibly breaking change: because of the default implementations of
  some functions (see below), the `Expected`, `ExpectedWithError`, and
  `ExpectedResult` traits now require a `Sized` type. This does not
  affect any of the default implementations shipped with the `expect-exit`
  crate (`Result`, `Option`, and now `bool`), since they are all `Sized`,
  but it might affect other implementations for user-defined types.
- Provide `Expected` and `ExpectedResult` implementations for
  the `bool` standard type.
- Provide a default implementation for some functions so that they do
  not need to be implemented for each type over and over again:
  - `Expected::expect_or_die()`
  - `Expected::expect_or_die_()`
  - `Expected::expect_or_exit()`
  - `Expected::expect_or_exit_()`
  - `Expected::or_die_()`
  - `Expected::or_exit_()`
  - `ExpectedWithError::expect_or_die_perror()`
  - `ExpectedWithError::expect_or_die_perror_()`
  - `ExpectedWithError::expect_or_exit_perror()`
  - `ExpectedWithError::expect_or_exit_perror_()`
  - `ExpectedWithError::or_die_e_()`
  - `ExpectedWithError::or_exit_e_()`
  - `ExpectedResult::expect_result()`
  - `ExpectedResult::expect_result_()`
  - `ExpectedResult::expect_result_nb_()`
- This means that the only functions that need to be implemented for
  the respective traits are now as follows:
  - `Expected::or_die()`
  - `Expected::or_exit()`
  - `ExpectedWithError::or_die_e()`
  - `ExpectedWithError::or_exit_e()`
  - `ExpectedResult::expect_result_nb()`
- Do some inner refactoring to honor some of Clippy's suggestions.
  Add a `run-clippy.sh` tool to perform some pedantic checks.
- Correct a typographical error in a previous changelog entry.

## 0.4.3 (2022-06-04)

- Add an EditorConfig definitions file.
- Add the "categories" and "keywords" package metadata fields.
- Activate another Clippy lint.

## 0.4.2 (2022-04-27)

- Declare Rust edition 2021 with no changes.
- Rename the test binary to `test_expect_exit` and drop the invocation
  code from it; the tests are only invoked via `cargo test` now.
- Document the errors returned by public functions.
- Document private functions, constants, structs, and enums, too.
- Mark most functions as inline.
- Import struct names directly.
- Honor some other Clippy suggestions.

## 0.4.1 (2021-09-01)

- Make the `html_root_url` attribute a crate-level one, as spotted by
  the 1.54.0 compiler.
- Drop `mod lib` from the test program's `main.rs` file; it should consume
  the crate as it is supposed to be used, not look into its internals.
  Thanks to John Hodge on IRC!
- Only document the "test" mode in the test program's usage message.

## 0.4.0 (2021-06-22)

- Make the example program in the documentation compile.
- Add the `html_root_url` documentation attribute.
- Rename the methods, hopefully for the last time.

## 0.3.1 (2021-05-21)

- Add methods that take callback functions to build the displayed message.
- Document the functions and the methods.
- Implement some suggestions from the clippy tool.
- Add shorter method aliases.

## 0.3.0 (2021-01-19)

- Add the `ExpectedResult` trait.

## 0.2.0 (2020-11-13)

- Add the `ExpectedWithError` trait.
- Add functions that do not unwind the stack.
- Add a self-test binary package.
- Rename the functions and methods.

## 0.1.0 (2020-11-12)

- First public release.

Peter Pentchev <[roam@ringlet.net](mailto:roam@ringlet.net)>
