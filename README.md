<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# expect-exit: display an error message and exit without a panic

*Note:* This library is deprecated, and it is in maintenance mode.
The `anyhow` library may be a better choice for both new and existing programs.

The `expect-exit` library defines the `Expected`, `ExpectedWithError`,
and `ExpectedResult` traits and implements them for the standard
`Result` and `Option` types as appropriate. This allows a program to
display an error message and exit with a non-zero exit code without
invoking a Rust panic, yet optionally unwinding the stack so that
various objects may perform some clean-up actions.

The methods with an `_e` suffix append an appropriate error message to
the supplied one. The methods with a `_` suffix allow the caller to
specify an already-constructed message instead of a function that
returns it.

## Example

    use expect_exit::{Expected, ExpectedResult};
    
    {
        env::var(name).or_exit(|| format!("{} not specified in the environment", name))
    
        fs::read_to_string(path).or_exit_e(|| format!("Could not read {:?}", path))
    
        tx.send(result).await.or_exit_e_("Could not tell the main thread");
    
        let config = parse().expect_result_("Could not parse the config")?;
        Ok(config.value + 1)
    }

For the crate's change history, see
the [NEWS.md](https://gitlab.com/ppentchev/expect-exit/-/blob/master/NEWS.md) 
file in the source distribution.

Author: Peter Pentchev <[roam@ringlet.net](mailto:roam@ringlet.net)>
